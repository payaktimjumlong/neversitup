//
//  Localizer.swift
//  NeversitupTodo
//
//  Created by essindy on 12/5/2564 BE.
//

import Foundation

enum AvailableLanguage: String {
    case thai = "th"
    case english = "en"
}

protocol LocalizerProtocol {
    func getCurrentLanguage() -> AvailableLanguage
    func setCurrentLanguage(_ lang: AvailableLanguage)
    func stringForKey(key: String) -> String
}

class Localizer: LocalizerProtocol {
    static let `default` = Localizer()
    static let userDefaultsKey = "InterfaceLanguage"

    let defaultLanguage = AvailableLanguage.thai
    var bundle: Bundle = .main
    
    init() {
        setBundle()
    }
    
    func getCurrentLanguage() -> AvailableLanguage {
        if let loaded = UserDefaults.standard.object(forKey: Localizer.userDefaultsKey) as? String, let current = AvailableLanguage.init(rawValue: loaded) {
            return current
        }
        return defaultLanguage
    }
    
    func setCurrentLanguage(_ lang: AvailableLanguage) {
        if lang != getCurrentLanguage() {
            UserDefaults.standard.set(lang.rawValue, forKey: Localizer.userDefaultsKey)
            UserDefaults.standard.synchronize()
            setBundle()
        }
    }
    
    func stringForKey(key: String) -> String {
        return self.bundle.localizedString(forKey: key, value: nil, table: nil)
    }
    
    private func setBundle() {
        let lang = getCurrentLanguage()
        if let path = Bundle.main.path(forResource: lang.rawValue, ofType: "lproj"), let bundle = Bundle(path: path) {
            self.bundle = bundle
        }
    }
}
