//
//  UIButton+Localizer.swift
//  NeversitupTodo
//
//  Created by essindy on 12/5/2564 BE.
//

import UIKit

class LocalizedButton: UIButton {
    @IBInspectable var localizationKey: String? {
        didSet {
            DispatchQueue.main.async {
                self.setTitle(self.localizationKey?.localized(), for: .normal)
            }
        }
    }   
}
