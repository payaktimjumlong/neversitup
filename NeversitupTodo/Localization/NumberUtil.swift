//
//  NeversitupTodo
//
//  Created by essindy on 12/5/2564 BE.
//

import Foundation

class NumberUtil {
    static func formatDecimal(_ str: String, withDecimal: Bool = true) -> String {
        let stringNumberForFormat = removeDoubleDot(str)
        let num = Double(stringNumberForFormat.replacingOccurrences(of: ",", with: ""))
        if withDecimal {
            return formatDecimal(num ?? 0.00)
        }
        return formatDecimal(num ?? 0.00, formatter: NumberFormatter.Style.decimal)
    }

    static func formatDecimal(_ num: Double) -> String {
        formatDecimal(num, formatter: NumberFormatter.Style.currency)
    }

    static func formatDecimal(_ num: Double, formatter: NumberFormatter.Style) -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = formatter
        numberFormatter.currencySymbol = ""
        return numberFormatter.string(from: NSNumber(value: num))!
    }

    static private func isDoubleDot(_ str: String) -> Bool {
        str.contains("..")
    }

    static func removeDoubleDot(_ str: String) -> String {
        if isDoubleDot(str) {
            return str.replacingOccurrences(of: "..", with: ".")
        }
        return str
    }

    static func removeDot(_ str: String) -> String {
        if isDoubleDot(str) {
            return str.replacingOccurrences(of: ".", with: "")
        }
        return str
    }

    static func removeComma(_ str: String) -> String {
        str.replacingOccurrences(of: ",", with: "")
    }

    static func formatAmount(_ str: String) -> String {
        let stringAmountForFormat = removeDoubleDot(str)
        if stringAmountForFormat.count == 0 {
            return ""
        }
        var stringAmountFormat = "0.00"
        let lastCharIsDot = stringAmountForFormat.last == "."

        let arrayAmount = stringAmountForFormat.components(separatedBy: ".")
        if arrayAmount.count == 2 {
            stringAmountFormat = formatDecimal(String(arrayAmount[0]), withDecimal: false)
            stringAmountFormat += "." + String(arrayAmount[1])
        } else {
            stringAmountFormat = formatDecimal(String(stringAmountForFormat), withDecimal: false)
        }

        if lastCharIsDot {
            stringAmountFormat += "."
        }

        return removeDoubleDot(stringAmountFormat)
    }
    
    static func validateAmountValue(amountValue: String, maxValue: Double) -> Bool {
        let numeric = amountValue.filter("0123456789.".contains)
        guard let casted = Double(numeric) else {
            return  numeric == "." || numeric == ""
        }
        if isMoreThanTwoDecimalPlaces(str: numeric) {
            return false
        }
        return maxValue >= casted
    }
    
    static func validateAccountNumber(accountNumber: String, maxLength: Int) -> Bool {
        return maxLength >= accountNumber.filter("1234567890".contains).count
    }
    
    static private func isMoreThanTwoDecimalPlaces(str: String) -> Bool {
        let arr = str.split(separator: ".")
        if arr.count > 1 && arr[1].count > 2 {
            return true
        }
        return false
    }
}
