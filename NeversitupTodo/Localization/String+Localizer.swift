//
//  Localizer+String.swift
//  NeversitupTodo
//
//  Created by essindy on 12/5/2564 BE.
//

import Foundation


extension String {
    func localized(localizer: LocalizerProtocol = Localizer.default) -> String {
        return localizer.stringForKey(key: self)
    }
}
