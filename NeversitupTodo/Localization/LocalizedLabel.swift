//
//  Localizer+UILabel.swift
//  NeversitupTodo
//
//  Created by essindy on 12/5/2564 BE.
//

import UIKit

class LocalizedLabel: UILabel {
    @IBInspectable var localizationKey: String? {
        didSet {
            DispatchQueue.main.async {
                self.text = self.localizationKey?.localized()
            }
        }
    }
}
