//
//  UITextField+Localizer.swift
//  NeversitupTodo
//
//  Created by essindy on 12/5/2564 BE.
//

import UIKit

class LocalizedTextField: UITextField {
    @IBInspectable var localizationKey: String? {
        didSet {
            DispatchQueue.main.async {
                let placeholder = self.localizationKey?.localized()
                let attribute = NSAttributedString(string: placeholder!, attributes: [
                    .foregroundColor: UIColor(red: 0.639, green: 0.639, blue: 0.741, alpha: 1)
                ])
                self.attributedPlaceholder = attribute
            }
        }
    }
}
