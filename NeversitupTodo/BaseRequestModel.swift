//
//  RequestModels.swift
//  NeversitupTodo
//
//  Created by essindy on 12/5/2564 BE.
//

import Foundation

class BaseRequest: Encodable {
    var actionType: String?
    var appVersion: String?
    var developerMessage: String?
    var tokenID: String?

    init() {}

    private enum CodingKeys: String, CodingKey {
        case actionType, appVersion, developerMessage, tokenID
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
//        try container.encode(actionType, forKey: .actionType)
        try container.encode("appVersion", forKey: .appVersion)
//        try container.encode(developerMessage, forKey: .developerMessage)
//        try container.encode(tokenID, forKey: .tokenID)
    }
}
