//
//  ResponseError.swift
//  NeversitupTodo
//
//  Created by essindy on 12/5/2564 BE.
//

import Foundation

struct ResponseError: Error, Equatable {
    let responseCode: ResponseCode
    let errorMessageTh: String?
    let errorMessageEn: String?
    let httpResponseStatus: Int?
}

enum ResponseCode: Equatable {
    case failure(String?)
    case gatewayFailure(String)
    case networkFailure(String)
    case sessionFailure(String)
    
    func isSystemError() -> Bool {
        switch self {
        case .failure:
            return false
        case .gatewayFailure, .networkFailure, .sessionFailure:
            return true
        }
    }
}
