//
//  BaseResponseModel.swift
//  NeversitupTodo
//
//  Created by essindy on 12/5/2564 BE.
//

import Foundation

// swiftlint:disable identifier_name
struct ResponseStatus: Decodable {
    let developerMessage: String?
    let httpStatus: Int?
    let responseCode: String?
    let responseMessage: String?
    let responseNamespace: String?
    let errorMessageTh: String?
    let errorMessageEn: String?
}

struct BaseResponse<T>: Decodable where T: Decodable {
    let language: String?
    let responseStatus: ResponseStatus
    let value: T?
}

struct ResultResponse<T>: Decodable where T: Decodable {
    let result: BaseResponse<T>?
    let error_response: ErrorResponse?
}

struct ErrorResponse: Decodable {
    let language: String?
    let responseStatus: ResponseStatus?
    let error_message: String?
    let error_code: String?
    let url_ios: String?
}
