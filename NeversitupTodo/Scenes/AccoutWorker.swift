//
//  AccoutWorker.swift
//  NeversitupTodo
//
//  Created by essindy on 12/5/2564 BE.
//  Copyright (c) 2564 ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

//import UIKit
import Foundation

enum AccoutRequest {
    
    class LoginRequest: BaseRequest {
        
        var email: String?
        var password: String?
        
        override init() {
            super.init()
        }
        
        override func encode(to encoder: Encoder) throws {
            try super.encode(to: encoder)
            var container = encoder.container(keyedBy: CodingKeys.self)
            try container.encode(email, forKey: .email)
            try container.encode(password, forKey: .password)
        }
        
        struct Response {
            let value: Bool?
            let error: ResponseError?
        }
        
        private enum CodingKeys: String, CodingKey {
            case email
            case password
        }
    }
    
    class Request: BaseRequest {
        
        var name: String?
        var email: String?
        var password: String?
        var age: String?
        
        override init() {
            super.init()
        }
        
        override func encode(to encoder: Encoder) throws {
            try super.encode(to: encoder)
            var container = encoder.container(keyedBy: CodingKeys.self)
            try container.encode(name, forKey: .name)
            try container.encode(email, forKey: .email)
            try container.encode(password, forKey: .password)
            try container.encode(age, forKey: .age)
        }
        
        struct Response {
            let value: Bool?
            let error: ResponseError?
        }
        
        private enum CodingKeys: String, CodingKey {
            case name
            case email
            case password
            case age
        }
    }
}

protocol AccoutWorkerProtocol {
    func registerAccout(accoutRequest: AccoutRequest.Request, completionHandler: @escaping (Result<Any, ResponseError>) -> Void)
    
    func loginAccout(accoutRequest: AccoutRequest.LoginRequest, completionHandler: @escaping (Result<Any, ResponseError>) -> Void)
    
}

final class AccoutVerifyWorker: BaseWorker, AccoutWorkerProtocol {
    func loginAccout(accoutRequest: AccoutRequest.LoginRequest, completionHandler: @escaping (Result<Any, ResponseError>) -> Void) {
        do {
            let parameters = try JSONSerialization.jsonObject(with: try JSONEncoder().encode(accoutRequest), options: []) as? [String: Any]
            
            let endpoint = Endpoint(path: "user/login", bodyParameters: parameters!)
            
            apiClient.request(endpoint: endpoint, of: [Accout.Login.Response].self, completion: completionHandler)
            
        } catch {
            completionHandler(.failure(ResponseError(responseCode: .failure("ErrorFromWorker"), errorMessageTh: nil, errorMessageEn: nil, httpResponseStatus: nil)))
        }
    }
    
    func registerAccout(accoutRequest: AccoutRequest.Request, completionHandler: @escaping (Result<Any, ResponseError>) -> Void) {
        do {
            let parameters = try JSONSerialization.jsonObject(with: try JSONEncoder().encode(accoutRequest), options: []) as? [String: Any]
            
            let endpoint = Endpoint(path: "user/register", bodyParameters: parameters!)
            
            apiClient.request(endpoint: endpoint, of: [Accout.Verification.Response].self, completion: completionHandler)
            
        } catch {
            completionHandler(.failure(ResponseError(responseCode: .failure("ErrorFromWorker"), errorMessageTh: nil, errorMessageEn: nil, httpResponseStatus: nil)))
        }
    }
    
}
