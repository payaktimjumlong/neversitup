//
//  APIClient.swift
//  NeversitupTodo
//
//  Created by essindy on 12/5/2564 BE.
//

import Foundation
import Alamofire
import AlamofireNetworkActivityLogger

protocol APIClientService {
    typealias CompletionHandler<T> = (Result<T, ResponseError>) -> Void

    func request<T: Decodable>(endpoint: Endpoint, of: T.Type, completion: @escaping CompletionHandler<Any>)
}

final class DefaultAPIClientService {
    private let decoder: JSONDecoder = JSONDecoder()
    private let closeSystemCode: String = "1002"
    private let forceUpdateCode: String = "1003"
}

extension DefaultAPIClientService: APIClientService {

    func request<T>(endpoint: Endpoint, of: T.Type, completion: @escaping (Result<Any, ResponseError>) -> Void) where T: Decodable {
        let appConfigurations = AppConfigurations()
        let accessToken = "Bearer \(NetworkSessions.shared.accessToken ?? "")"
        let headers: HTTPHeaders = [
            "Authorization": accessToken,
            "Accept": "application/json",
            "Content-Type": "application/json"
        ]

        var url = "\(appConfigurations.apiBaseURL)/"
        url.append(endpoint.path)
        
//        print("url = \(url)")
//        print("endpoint.bodyParameters = \(endpoint.bodyParameters)")
        NetworkActivityLogger.shared.level = .debug
        NetworkActivityLogger.shared.startLogging()
        
        AF.request(URL.init(string: url)!,
                   method: .post,
                   parameters: endpoint.bodyParameters,
                   encoding: JSONEncoding.default,
                   headers: headers).responseJSON { (response) in
                print(response.result)

                switch response.result {

                case .success(_):
                    if let json = response.value
                    {
                        print("request success : \(json)")
                        completion(.success(json))
                    }
                    break
                case .failure(let error):
                    print("request  failure : \(error)")
                    let msg = error.errorDescription?.description
                    let error = ResponseError(responseCode: .networkFailure("ERROR"), errorMessageTh: msg, errorMessageEn: msg, httpResponseStatus: response.response?.statusCode)
                        completion(.failure(error))
                    break
                }
            }
        
        
//        ApiDataNetworkConfig.session.request(url,
//                method: .post,
//                parameters: endpoint.bodyParameters,
//                encoding: JSONEncoding.default,
//                headers: headers
//        ).responseDecodable(of: ResultResponse<T>.self, decoder: decoder) { response in
//            switch response.result {
//            case .success(let data):
//                print("response : \(response)")
//                print("response data : \(data)")
//                print("response data.result : \(String(describing: data.result))")
//                print("response data.result?.value : \(String(describing: data.result?.value))")
////                guard let result = data.result else {//Rib error,Kraken error
////                let errorResponse = data.error_response
////
////                    return
////                }
//                let result = data.result
//                let value = result?.value
//                print("value = \(String(describing: value))")
////                guard let value = result?.value else {
////                    print("Backend error \(String(describing: result))")
////                    let error = ResponseError(responseCode: .failure(result?.responseStatus.responseCode), errorMessageTh: result?.responseStatus.errorMessageTh, errorMessageEn: result?.responseStatus.errorMessageEn, httpResponseStatus: response.response?.statusCode)
////
////                    completion(.failure(error))
////                    return
////                }
//
//                completion(.success(value!))
//            case .failure(let error):
//                print("response failure : \(error)")
//                let msg = error.errorDescription?.description
//                let error = ResponseError(responseCode: .networkFailure("ERROR"), errorMessageTh: msg, errorMessageEn: msg, httpResponseStatus: response.response?.statusCode)
//                completion(.failure(error))
//            }
//        }
    }
    
    
    
}

public protocol DataTransferErrorLogger {
    func log(error: Error)
}

// MARK: - Logger
public final class DefaultDataTransferErrorLogger: DataTransferErrorLogger {
    public init() {
    }

    public func log(error: Error) {
        #if DEBUG
        print("-------------")
        print("\(error)")
        #endif
    }
}

struct Certificates {
    static let sitCert = Certificates.certificate(filename: "api-nodejs-todolist.herokuapp.com")
    static let uatCert = Certificates.certificate(filename: "api-nodejs-todolist.herokuapp.com")
    static let prdCert = Certificates.certificate(filename: "api-nodejs-todolist.herokuapp.com")

    private static func certificate(filename: String) -> SecCertificate {
        guard let filePath = Bundle.main.path(forResource: filename, ofType: "der") else {
            fatalError("No Certificates")
        }
        let data: Data?
        do {
            data = try Data(contentsOf: URL(fileURLWithPath: filePath))
        } catch {
            data = nil
        }
        guard let dataNotNil = data else {
            fatalError("No Certificates Data")
        }
        guard let certificate = SecCertificateCreateWithData(nil, dataNotNil as CFData) else {
            fatalError("No SecCertificateCreateWithData")
        }

        return certificate
    }
}
