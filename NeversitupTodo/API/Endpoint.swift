//
//  Endpoint.swift
//  NeversitupTodo
//
//  Created by essindy on 12/5/2564 BE.
//

import Foundation
public class Endpoint {

    public var path: String
    public var bodyParameters: [String: Any]

    init(path: String,
         bodyParameters: [String: Any] = [:]) {
        self.path = path
        self.bodyParameters = bodyParameters
    }
}
