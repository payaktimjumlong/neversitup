//
//  AppConfigurations.swift
//  NeversitupTodo
//
//  Created by essindy on 12/5/2564 BE.
//

import Foundation
final class AppConfigurations {
    lazy var apiBaseURL: String = {
        guard let apiBaseURL = Bundle.main.object(forInfoDictionaryKey: "ApiBaseURL") as? String else {
            fatalError("ApiBaseURL must not be empty in plist")
        }
        return apiBaseURL
    }()
    
    lazy var apiBaseVersion: String = {
        guard let apiBaseURL = Bundle.main.object(forInfoDictionaryKey: "ApiBaseVersion") as? String else {
            fatalError("ApiBaseVersion must not be empty in plist")
        }
        return apiBaseURL
    }()

    lazy var isProd: Bool = {
        guard let isProdStr = Bundle.main.object(forInfoDictionaryKey: "IsProd") as? String else {
            fatalError("isProd must not be empty in plist")
        }
        
        var isProdBool: Bool {
            switch isProdStr.lowercased() {
            case "true":
                return true
            case "false":
                return false
            default:
                return false
            }
        }
        return isProdBool
    }()
    
//    lazy var isAnalytics: Bool = {
//        guard let isAnalyticsStr = Bundle.main.object(forInfoDictionaryKey: "IsAnalytics") as? String else {
//            fatalError("IsAnalytics must not be empty in plist")
//        }
//        
//        var isAnalyticsBool: Bool {
//            switch isAnalyticsStr.lowercased() {
//            case "true":
//                return true
//            case "false":
//                return false
//            default:
//                return false
//            }
//        }
//        return isAnalyticsBool
//    }()
}
