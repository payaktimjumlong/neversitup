//
//  BaseWoker.swift
//  NeversitupTodo
//
//  Created by essindy on 12/5/2564 BE.
//

import UIKit
import Foundation
import JNKeychain

class BaseWorker {
    let apiClient: APIClientService
    let headerRequestService: HeaderRequestService
    let platformService: PlatformService
    
    init(apiClient: APIClientService = DefaultAPIClientService(),
         headerRequestService: HeaderRequestService = HeaderRequestService(),
         platformService: PlatformService = PlatformService()
    ) {
        self.apiClient = apiClient
        self.headerRequestService = headerRequestService
        self.platformService = platformService
    }
}

struct HeaderModel: Codable {
    var transactionDateTime: String?
    var transactionDate: String?
    var serviceName: String?
    var systemCode: String?
    var referenceNo: String?
    var channelID: String?
}

struct PlatformModel: Codable {
    var deviceType: String?
    var deviceToken: String?
    var deviceName: String?
    var deviceModel: String?
    var deviceUUID: String?
    var osname: String?
    var osName: String?
    var osversion: String?
    var osVersion: String?
    var isIllegal: String?
}

class HeaderRequestService {

    public func getRequestHeader() -> HeaderModel {
        let transactionDate = DateTimeUtil.formatDate(pattern: DateTimeUtil.Format.YYYYMMDDHHMMSS)
        let rnd = Int(arc4random_uniform(998))+1
        var header = HeaderModel()
        header.transactionDateTime = "\(DateTimeUtil.getCurrentDate())"
        header.transactionDate = transactionDate
        header.referenceNo = "\(transactionDate)\(rnd)"
        return header
    }
}

class PlatformService {
//    keychainManagerService: KeychainManagerService = KeychainManagerService()
    
    lazy var currentDevice: UIDevice = {
        return UIDevice.current
    }()

    lazy var OSVersion: String = {
       let os = ProcessInfo().operatingSystemVersion
        return "\(os.majorVersion).\(os.minorVersion).\(os.patchVersion)"
    }()
    
    lazy var keychainManagerService: KeychainManagerService = {
        return KeychainManagerService()
    }()

    public func getDeviceInfo() -> PlatformModel {
        
        guard let deviceUUID = keychainManagerService.getDeviceIdentifierFromKeychain() else {
            fatalError("Can't get device id")
        }

        var platform = PlatformModel()
        platform.deviceType   = currentDevice.model
        platform.deviceToken  = "deviceToken"
//        platform.deviceName   = UIDevice.modelName
//        platform.deviceModel  = UIDevice.modelName
        platform.deviceUUID   = deviceUUID
        platform.osname       = currentDevice.systemName
        platform.osName       = currentDevice.systemName
        platform.osVersion    = OSVersion
        platform.osversion    = OSVersion
        platform.isIllegal    = isJailbroken() ? "TRUE" : "FALSE"
        return platform
    }

    private func isJailbroken() -> Bool {
        #if arch(i386) || arch(x86_64)
            return false
        #else
        let fm = FileManager.default

        if fm.fileExists(atPath: "/private/var/lib/apt") {
                // This Device is jailbroken
                return true
            } else {
                // Continue the device is not jailbroken
                return false
            }
        #endif
    }
}

class KeychainManagerService: NSObject {
    func getDeviceIdentifierFromKeychain() -> String? {

        // try to get value from keychain
        var deviceUDID = self.keychain_valueForKey("keychainDeviceUDID") as? String
        if deviceUDID == nil {
            deviceUDID = UIDevice.current.identifierForVendor!.uuidString
            // save new value in keychain
            self.keychain_setObject(deviceUDID! as AnyObject, forKey: "keychainDeviceUDID")
        }
        return deviceUDID!
    }

    // MARK: - Keychain

    func keychain_setObject(_ object: AnyObject, forKey: String) {
        let result = JNKeychain.saveValue(object, forKey: forKey)
        if !result {
            print("keychain saving: smth went wrong")
        }
    }

    func keychain_deleteObjectForKey(_ key: String) -> Bool {
        let result = JNKeychain.deleteValue(forKey: key)
        return result
    }

    func keychain_valueForKey(_ key: String) -> AnyObject? {
        let value = JNKeychain.loadValue(forKey: key)
        return value as AnyObject?
    }
}

