//
//  NetworkSessions.swift
//  NeversitupTodo
//
//  Created by essindy on 12/5/2564 BE.
//

import Foundation

class NetworkSessions {

    static let shared = NetworkSessions()

    var accessToken: String?
    var sessionToken: String?
    var hasMultipleAccounts: Bool = false

    // Initializer access level change now
    private init() {}
}
