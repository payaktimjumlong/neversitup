//
//  DateTimeUtil.swift
//  NeversitupTodo
//
//  Created by essindy on 12/5/2564 BE.
//

import Foundation

public struct DateTimeUtil {
    
    @available(*, unavailable) private init() {}
    
    enum Format: String {
        case DDMMYYYYHHMMSS = "dd/MM/yyyy HH:mm:ss"
        case DDMMYYYYHHMM = "dd/MM/yyyy HH:mm"
        case DDMMYYYY = "dd/MM/yyyy"
        case DDMMMYY = "dd MMM yy"
        case DDMMMYYYY = "dd MMM YYYY"
        case DDMMMYYHHMM = "dd MMM yy HH:mm"
        case YYYYMMDDHHMMSS = "yyyyMMddHHmmss"
        case YYYYMMDDHHMMSSUTC = "yyyy-MM-dd HH:mm:ss.SSSS"
        case YYYYMMDD = "yyyy-MM-dd"
        case YYYYMM = "yyyy-MM"
        case HHMMSS = "HH:mm:ss"
        case HHMM = "HH:mm"
        case YYYYMMDDTHHMMSSZ = "yyyy-MM-dd'T'HH:mm:ssZ"
        case YYYYMMDDTHHMMSSSSSZ = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        case MMMM = "MMMM"
        case YYYY = "YYYY"
        case yyyy = "yyyy"
        case yyyyMMdd = "yyyyMMdd"
    }
    
    static func getCurrentDate() -> Date {
        let dt = Date()
        return dt
    }
    
    static func formatDate(dt: Date = getCurrentDate(), pattern: Format = Format.DDMMYYYYHHMMSS, locale: String = "locale".localized()) -> String {
        let df = DateFormatter()
        df.timeZone = TimeZone(abbreviation: "ICT")
        df.dateFormat = pattern.rawValue
        df.locale = Locale(identifier: locale)
        
        let str = df.string(from: dt)
        return str
    }
    
    static func formatDate(date: String, from: Format, to: Format, locale: String = "locale".localized(), toTz: TimeZone = TimeZone(abbreviation: "ICT")!) -> String {
        guard let formatDate = parseDate(date: date, pattern: from) else { return "" }
        
        let dateFormatPrint = DateFormatter()
        dateFormatPrint.timeZone = toTz
        dateFormatPrint.locale = Locale(identifier: locale)
        dateFormatPrint.dateFormat = to.rawValue
        return dateFormatPrint.string(from: formatDate)
    }
    
    static func formatDateToTH(date: String, fromPattern: Format, toPattern: Format) -> String {
        guard let formatDate = parseDate(date: date, pattern: fromPattern) else { return "" }
        
        let dateFormatPrint = DateFormatter()
        dateFormatPrint.timeZone = TimeZone(abbreviation: "ICT")
        dateFormatPrint.locale = Locale(identifier: "th_TH")
        //        dateFormatPrint.dateFormat = toPattern.rawValue
        dateFormatPrint.setLocalizedDateFormatFromTemplate(toPattern.rawValue)
        return dateFormatPrint.string(from: formatDate)
    }
    
    static func parseDate(date: String, pattern: Format) -> Date? {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = pattern.rawValue
        dateFormat.locale = Locale(identifier: "us_US")
        let dt = dateFormat.date(from: date)
        return dt
    }
   
    static func parseDate(date: String, pattern: Format, locale: String = "locale".localized(), toTz: TimeZone = TimeZone(abbreviation: "ICT")!) -> Date? {
       
        let dateFormat = DateFormatter()
        dateFormat.timeZone = toTz
        dateFormat.locale = Locale(identifier: locale)

        dateFormat.dateFormat = pattern.rawValue
        
        let dt = dateFormat.date(from: date)
        return dt
    }
    
    static func formatDateToTH(date: Date, pattern: Format, locale: String = "locale".localized(), toTz: TimeZone = TimeZone(abbreviation: "ICT")!) -> String {
        
        let dateFormatPrint = DateFormatter()
        dateFormatPrint.timeZone = toTz
        dateFormatPrint.locale = Locale(identifier: locale)
        dateFormatPrint.dateFormat = pattern.rawValue
        dateFormatPrint.locale = Locale(identifier: locale)
        
        return dateFormatPrint.string(from: date)
    }
        
    static func timeAgoStringFromDate(dateCompare: Date, serverDateNow: Date = Date()) -> String? {
        let formatter = DateComponentsFormatter()
        formatter.unitsStyle = .full
        
        var calendar = NSCalendar.current
        calendar.locale = Locale(identifier: "locale".localized())
        formatter.calendar = calendar
        let components1: Set<Calendar.Component> = [.year, .month, .weekOfMonth, .day, .hour, .minute, .second]
        let components = calendar.dateComponents(components1, from: dateCompare, to: serverDateNow)
        
        if components.year ?? 0 > 0 {
            formatter.allowedUnits = .year
        } else if components.month ?? 0 > 0 {
            formatter.allowedUnits = .month
        } else if components.weekOfMonth ?? 0 > 0 {
            formatter.allowedUnits = .weekOfMonth
        } else if components.day ?? 0 > 0 {
            formatter.allowedUnits = .day
        } else if components.hour ?? 0 > 0 {
            formatter.allowedUnits = [.hour]
        } else if components.minute ?? 0 > 0 {
            formatter.allowedUnits = .minute
        } else {
            formatter.allowedUnits = .second
        }
        
        let formatString = NSLocalizedString("%@ \("ago".localized())", comment: "Used to say how much time has passed. e.g. '2 hours ago'")
        
        guard let timeString = formatter.string(for: components) else {
            return nil
        }
        return String(format: formatString, timeString)
    }
}
