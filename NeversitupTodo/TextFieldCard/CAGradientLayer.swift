//
//  CALayer.swift
//  NeversitupTodo
//
//  Created by essindy on 12/5/2564 BE.
//

import UIKit

extension CAGradientLayer {
    func setStartAndEndPoint(angle: Double) {
        let angleRatio = angle / 360.0
        let startX = pow(sinf(Float(Double(2 * Double.pi * ((angleRatio + 0.75) / 2)))), 2)
        let startY = pow(sinf(Float(Double(2 * Double.pi * (angleRatio / 2)))), 2)
        let endX = pow(sinf(Float(Double(2 * Double.pi * ((angleRatio + 0.25) / 2)))), 2)
        let endY = pow(sinf(Float(Double(2 * Double.pi * ((angleRatio + 0.5) / 2)))), 2)
        
        self.endPoint = CGPoint(x: CGFloat(endX), y: CGFloat(endY))
        self.startPoint = CGPoint(x: CGFloat(startX), y: CGFloat(startY))
    }
}
