//
//  FormTextFieldButton.swift
//  NeversitupTodo
//
//  Created by essindy on 12/5/2564 BE.
//

import UIKit

@IBDesignable
class FormTextFieldButton: UIControl {
    var drawLayers: [CALayer] = []
    var imageView: UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupImageView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupImageView()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setupImageView()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        setup()
    }
    
    private func setup() {
        self.clipsToBounds = false
        self.backgroundColor = .clear
        updateLayers()
    }
    
    func setImage(image: UIImage?) {
        self.imageView.image = image
    }
    
    private func setupImageView() {
        self.imageView = UIImageView(image: nil)
        self.addSubview(imageView)
        imageView.isUserInteractionEnabled = false
        imageView.contentMode = .center
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.widthAnchor.constraint(equalToConstant: 32).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 32).isActive = true
        imageView.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        imageView.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
    }
    
    private func updateLayers() {
        drawLayers.forEach { $0.removeFromSuperlayer() }
        drawLayers = []

        let path = UIBezierPath(roundedRect: self.bounds.inset(by: UIEdgeInsets(top: 0.5, left: 0.5, bottom: 0.5, right: 0.5)), byRoundingCorners: [.topRight, .bottomRight], cornerRadii: CGSize(width: 2.5, height: 2.5)).cgPath
        let bounds = CGRect(origin: CGPoint(x: 0, y: 0), size: self.bounds.size)
        
        let leftShadow = getShadow(bounds: bounds)
        drawLayers.append(leftShadow)

        let bgs = getBackground(bounds: bounds, maskPath: path)
        drawLayers.append(contentsOf: bgs)

        let border = getBorderLayer(bounds: bounds, path: path)
        drawLayers.append(border)

        for (index, layer) in drawLayers.enumerated() {
            self.layer.insertSublayer(layer, at: UInt32(index))
        }
    }
    
    private func getShadow(bounds: CGRect) -> CALayer {
        let path = UIBezierPath()
        path.move(to: CGPoint(x: bounds.maxX, y: 0))
        path.addLine(to: CGPoint(x: bounds.maxX, y: bounds.maxY))
        path.addLine(to: CGPoint(x: bounds.maxX + 3, y: bounds.maxY))
        path.addLine(to: CGPoint(x: bounds.maxX + 3, y: 0))
        path.addLine(to: CGPoint(x: bounds.maxX, y: 0))
        
        let shadow = CAShapeLayer()
        shadow.frame = CGRect(x: -1 * bounds.maxX, y: 0, width: bounds.maxX, height: bounds.maxY)
        shadow.bounds = bounds
        shadow.shadowPath = path.cgPath
        shadow.shadowColor = UIColor(red: 0.137, green: 0.078, blue: 0.275, alpha: 1).cgColor
        shadow.shadowRadius = 2.5
        shadow.shadowOffset = CGSize(width: 0, height: 0)
        shadow.shadowOpacity = 0.3
        shadow.masksToBounds = true
        
        return shadow
    }
    
    private func getBackground(bounds: CGRect, maskPath: CGPath) -> [CALayer] {
        var layers: [CALayer] = []
        let maskLayer = CAShapeLayer()
        maskLayer.path = maskPath
        maskLayer.fillColor = UIColor.black.cgColor
        
        let bgLayer = CALayer()
        bgLayer.frame = bounds
        bgLayer.backgroundColor = UIColor(named: "#F0F0F3")?.cgColor
        bgLayer.mask = maskLayer
        layers.append(bgLayer)
        
        let maskLayer2 = CAShapeLayer()
        maskLayer2.path = maskPath
        maskLayer2.fillColor = UIColor.black.cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = bounds
        gradientLayer.mask = maskLayer2
        
        gradientLayer.colors = [
            UIColor(red: 1, green: 1, blue: 1, alpha: 0.4).cgColor,
            UIColor(red: 1, green: 1, blue: 1, alpha: 0).cgColor
        ]
        
        gradientLayer.locations = [0, 1]
        gradientLayer.startPoint = CGPoint(x: 0.5, y: 0.0)
        gradientLayer.endPoint = CGPoint(x: 0.5, y: 1.0)
        
        layers.append(gradientLayer)
        return layers
    }
    
    private func getBorderLayer(bounds: CGRect, path: CGPath) -> CALayer {
        let border = CAGradientBorderLayer(strokeWidth: 1, borderPath: path)
        border.frame = self.bounds
        border.colors = [
            UIColor(red: 1, green: 1, blue: 1, alpha: 1).cgColor,
            UIColor(red: 1, green: 1, blue: 1, alpha: 1).cgColor,
            UIColor(red: 0.878, green: 0.859, blue: 0.922, alpha: 1).cgColor
        ]
        border.locations = [0, 0.35, 1]
        border.startPoint = CGPoint(x: 0.0, y: 0.0)
        border.endPoint = CGPoint(x: 1.0, y: 1.0)
        
        return border
    }
}
