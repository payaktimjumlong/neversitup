//
//  FormTextField.swift
//  NeversitupTodo
//
//  Created by essindy on 12/5/2564 BE.
//

import UIKit

@IBDesignable
class FormTextField: UITextField {
    let cornerRadius: CGFloat = 5
    let strokeWidth: CGFloat = 5
    let innerShadowOffsetY: CGFloat = 6
    var textRect: CGFloat = 8
    var textEdgeleft: CGFloat = 8
    
    override var isEnabled: Bool {
        didSet {
            updateAppearanceOnStateChanged()
        }
    }
    
    override var placeholder: String? {
        didSet {
            guard let ph = placeholder else {
                return
            }
            DispatchQueue.main.async {
                let attribute = NSAttributedString(string: ph, attributes: [
                    .foregroundColor: UIColor(named: "#A3A3BD")!
                ])
                self.attributedPlaceholder = attribute
            }
        }
    }
    
    var layers: [CALayer] = []
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initDefaults()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initDefaults()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        initDefaults()
    }
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        let content = bounds.inset(by: UIEdgeInsets(top: 0, left: textEdgeleft, bottom: 0, right: textRect))
        return content
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        let content = bounds.inset(by: UIEdgeInsets(top: 0, left: textEdgeleft, bottom: 0, right: textRect))
        return content
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        layers.forEach { $0.removeFromSuperlayer() }
        layers = []
        
        rerenderLayers()
    }
    
    private func initDefaults() {
        self.borderStyle = .none
        self.clipsToBounds = true
        self.layer.cornerRadius = cornerRadius
        self.backgroundColor = UIColor(named: "F0F0F3")
    }
    
    private func updateAppearanceOnStateChanged() {
        if isEnabled {
            self.backgroundColor = UIColor(named: "F0F0F3")
            self.textColor = UIColor(named: "635F98")
        } else {
            self.backgroundColor = UIColor(named: "E7E7ED")
            self.textColor = UIColor(named: "A8A8A8")
        }
        layoutSubviews()
    }
    
    private func rerenderLayers() {
        if state == .disabled {
            return
        }
        
        let innerShadow = CAShapeLayer()
        innerShadow.frame = self.bounds
        let path = UIBezierPath(rect: innerShadow.bounds.inset(by: UIEdgeInsets(
            top: strokeWidth + innerShadowOffsetY,
            left: -1 * strokeWidth,
            bottom: 0,
            right: -1 * strokeWidth
            )
        ))

        let cutout = UIBezierPath(rect: innerShadow.bounds).reversing()
        path.append(cutout)
        innerShadow.shadowPath = path.cgPath
        innerShadow.masksToBounds = true
        
        innerShadow.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.05).cgColor
        innerShadow.shadowOpacity = 1
        innerShadow.shadowRadius = 5
        self.layer.addSublayer(innerShadow)
        layers.append(innerShadow)
        
        let gradientLayer = CAGradientBorderLayer(strokeWidth: strokeWidth)
        gradientLayer.frame = self.bounds
        gradientLayer.colors = [
            UIColor(red: 0.881, green: 0.87, blue: 0.908, alpha: 1).cgColor,
            UIColor(red: 0.991, green: 0.988, blue: 1, alpha: 1).cgColor
        ]
        gradientLayer.setStartAndEndPoint(angle: 37.0)
        gradientLayer.locations = [0.40, 0.65]
        gradientLayer.cornerRadius = cornerRadius
        
        self.layer.addSublayer(gradientLayer)
        layers.append(gradientLayer)
    }
}
