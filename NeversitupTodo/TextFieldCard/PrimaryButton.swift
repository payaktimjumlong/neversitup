//
//  PrimaryButton.swift
//  NeversitupTodo
//
//  Created by essindy on 12/5/2564 BE.
//

import UIKit

@IBDesignable
class PrimaryButton: LocalizedButton {
    var layers: [CALayer] = []
    private let cornerRadius: CGFloat = 12
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setTextAndBackgroundImageForStates()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setTextAndBackgroundImageForStates()
    }
    
    func setTextAndBackgroundImageForStates() {
        self.backgroundColor = .clear

        self.setTitleColor(UIColor(red: 0.349, green: 0.31, blue: 0.455, alpha: 1), for: .highlighted)
        self.setTitleColor(UIColor(red: 0.596, green: 0.596, blue: 0.596, alpha: 1), for: .disabled)
        self.setTitleColor(UIColor(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setupButtonBackground()
    }
    
    override var isHighlighted: Bool {
        didSet {
            setupButtonBackground()
        }
    }
    
    override var isEnabled: Bool {
        didSet {
            setupButtonBackground()
        }
    }
    
    private func setupButtonBackground() {
        layers.forEach { $0.removeFromSuperlayer() }
        layers = []
        
        if self.isHighlighted {
            setupBackgroundOnHighlighted()
        } else if !self.isEnabled {
            setupBackgroundOnDisabled()
        } else {
            setupBackgroundOnNormal()
        }
    }
    
    private func setupBackgroundOnHighlighted() {
        self.backgroundColor = .clear
        self.layer.shadowOpacity = 0
        
        let backgroundLayer = CALayer()
        backgroundLayer.frame = self.bounds
        backgroundLayer.backgroundColor = UIColor(red: 0.94, green: 0.94, blue: 0.95, alpha: 1.00).cgColor
        backgroundLayer.cornerRadius = cornerRadius
        self.layer.insertSublayer(backgroundLayer, at: 0)
        layers.append(backgroundLayer)
        
        let strokeWidth: CGFloat = 3
        let innerShadowOffsetY: CGFloat = 4
        
        let whiteInnerShadow = CAShapeLayer()
        whiteInnerShadow.frame = self.bounds
        let whiteInnerShadowPath = UIBezierPath(rect: whiteInnerShadow.bounds.inset(by: UIEdgeInsets(
            top: 0,
            left: 0,
            bottom: strokeWidth + innerShadowOffsetY,
            right: strokeWidth + innerShadowOffsetY - 1
            )
        ))

        let whiteInnerShadowCutout = UIBezierPath(rect: whiteInnerShadow.bounds).reversing()
        whiteInnerShadowPath.append(whiteInnerShadowCutout)
        whiteInnerShadow.shadowPath = whiteInnerShadowPath.cgPath
        whiteInnerShadow.masksToBounds = true

        whiteInnerShadow.shadowColor = UIColor.white.cgColor
        whiteInnerShadow.shadowOpacity = 1
        whiteInnerShadow.shadowRadius = 5
        whiteInnerShadow.cornerRadius = cornerRadius
        self.layer.insertSublayer(whiteInnerShadow, above: backgroundLayer)
        layers.append(whiteInnerShadow)
        
        let blackInnerShadow = CAShapeLayer()
        blackInnerShadow.frame = self.bounds
        let blackInnerShadowPath = UIBezierPath(rect: blackInnerShadow.bounds.inset(by: UIEdgeInsets(
            top: strokeWidth + innerShadowOffsetY,
            left: strokeWidth + innerShadowOffsetY - 1,
            bottom: 0,
            right: 0
            )
        ))

        let blackInnerShadowCutout = UIBezierPath(rect: blackInnerShadow.bounds).reversing()
        blackInnerShadowPath.append(blackInnerShadowCutout)
        blackInnerShadow.shadowPath = blackInnerShadowPath.cgPath
        blackInnerShadow.masksToBounds = true
        
        blackInnerShadow.shadowColor = UIColor(red: 0.682, green: 0.682, blue: 0.753, alpha: 0.6).cgColor
        blackInnerShadow.shadowOpacity = 1
        blackInnerShadow.shadowRadius = 5
        blackInnerShadow.cornerRadius = cornerRadius
        self.layer.insertSublayer(blackInnerShadow, above: whiteInnerShadow)
        layers.append(blackInnerShadow)
        
        let purpleOverlayBackground = CALayer()
        purpleOverlayBackground.frame = self.bounds
        purpleOverlayBackground.backgroundColor = UIColor(red: 0.39, green: 0.37, blue: 0.60, alpha: 0.10).cgColor
        purpleOverlayBackground.cornerRadius = cornerRadius
        self.layer.insertSublayer(purpleOverlayBackground, above: blackInnerShadow)
        layers.append(purpleOverlayBackground)
        
        let borderPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: 12)
        let gradientBorder = CAGradientBorderLayer(strokeWidth: 5, borderPath: borderPath.cgPath)
        gradientBorder.frame = self.bounds
        gradientBorder.colors = [
            UIColor(red: 1, green: 1, blue: 1, alpha: 1).cgColor,
            UIColor(red: 1, green: 1, blue: 1, alpha: 1).cgColor,
            UIColor(red: 0.827, green: 0.808, blue: 0.890, alpha: 1).cgColor
        ]
        gradientBorder.locations = [0, 0.35, 1]
        gradientBorder.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradientBorder.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradientBorder.cornerRadius = cornerRadius
        self.layer.insertSublayer(gradientBorder, above: purpleOverlayBackground)
        layers.append(gradientBorder)
    }
    
    private func setupBackgroundOnDisabled() {
        self.backgroundColor = UIColor(red: 0.659, green: 0.659, blue: 0.659, alpha: 0.5)
        self.layer.shadowColor = UIColor(red: 0.483, green: 0.483, blue: 0.483, alpha: 0.5).cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 4.0)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 8
        self.layer.cornerRadius = cornerRadius
    }
  
    private func setupBackgroundOnNormal() {
        self.backgroundColor = .clear
        self.layer.shadowOpacity = 0
        
        let gradientLayer = CAGradientLayer()
         gradientLayer.frame = self.bounds
         gradientLayer.colors = [
           UIColor(red: 0.349, green: 0.31, blue: 0.455, alpha: 1).cgColor,
           UIColor(red: 0.137, green: 0.078, blue: 0.275, alpha: 1).cgColor
         ]
         gradientLayer.startPoint = CGPoint(x: 0.25, y: 0.5)
         gradientLayer.endPoint = CGPoint(x: 0.75, y: 0.5)
         gradientLayer.locations = [0.0, 1.0]
         gradientLayer.cornerRadius = cornerRadius
         self.layer.insertSublayer(gradientLayer, at: 0)
         layers.append(gradientLayer)
        
        let shadowLayer = getHoverShadow()
        
        self.layer.insertSublayer(shadowLayer, at: 0)
        layers.append(shadowLayer)
    }
    
    private func getHoverShadow() -> CALayer {
        let shadow = CAShapeLayer()
        shadow.frame = CGRect(x: 0, y: bounds.height, width: bounds.width, height: 13)
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 10, y: shadow.frame.height - 2))
        path.addLine(to: CGPoint(x: 20, y: 0))
        path.addLine(to: CGPoint(x: shadow.frame.width - 20, y: 0))
        path.addLine(to: CGPoint(x: shadow.frame.width - 10, y: shadow.frame.height - 2))
        shadow.opacity = 0.6
        shadow.shadowPath = path.cgPath
        shadow.shadowOpacity = 1
        shadow.shadowRadius = 10
        shadow.shadowColor = UIColor(red: 0.137, green: 0.078, blue: 0.275, alpha: 1).cgColor
        
        return shadow
    }
}
