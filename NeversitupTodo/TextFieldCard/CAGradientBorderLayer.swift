//
//  CAGradientBorderLayer.swift
//  NeversitupTodo
//
//  Created by essindy on 12/5/2564 BE.
//

import UIKit

class CAGradientBorderLayer: CAGradientLayer {
    private let borderLayer = CAShapeLayer()
    var strokeWidth: CGFloat = 0 {
        didSet {
            updateMask()
        }
    }
    private var priorityPath: CGPath?
    
    init(strokeWidth: CGFloat) {
        super.init()
        self.mask = borderLayer
        self.strokeWidth = strokeWidth
        self.updateMask()
    }
    
    init(strokeWidth: CGFloat, borderPath: CGPath) {
        priorityPath = borderPath
        super.init()
        self.mask = borderLayer
        self.strokeWidth = strokeWidth
        self.updateMask()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func layoutSublayers() {
        super.layoutSublayers()
        updateMask()
    }
    
    func updateMask() {
        borderLayer.path = priorityPath ?? UIBezierPath(roundedRect: self.bounds, cornerRadius: cornerRadius).cgPath
        borderLayer.lineWidth = strokeWidth
        borderLayer.strokeColor = UIColor.black.cgColor
        borderLayer.fillColor = nil
    }
}
