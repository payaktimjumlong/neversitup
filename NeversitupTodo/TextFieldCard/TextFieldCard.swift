//
//  TextFieldCard.swift
//  NeversitupTodo
//
//  Created by essindy on 12/5/2564 BE.
//

import UIKit

enum TextFieldCardButtonType: Int {
    case hidden = 0
    case dropdown = 1
    case date = 2
}

@IBDesignable
class TextFieldCard: UIView {
    @IBOutlet weak var textField: FormTextField!
    @IBOutlet weak var mainLabel: LocalizedLabel!
    @IBOutlet weak var secondaryLabel: LocalizedLabel!
    @IBOutlet weak var asteriskLabel: UILabel!
    @IBOutlet weak var button: FormTextFieldButton!
    @IBOutlet weak var infoButton: UIButton!
    @IBOutlet weak var secondaryLabelLeadingConstraint: NSLayoutConstraint!
    
    private var amountValue = ""
    private let maxAmount = 99999999.99
    
    override var accessibilityIdentifier: String? {
        didSet {
            if let id = accessibilityIdentifier {
                textField.accessibilityIdentifier = "\(id)TextField"
            } else {
                textField.accessibilityIdentifier = "TextFieldInTextFieldCard"
            }
        }
    }
    
    @IBInspectable var enableTextField: Bool = true {
        didSet {
            textField.isEnabled = enableTextField
        }
    }
    
    @IBInspectable var labelLocalizationKey: String = "" {
        didSet {
            self.mainLabel.text = labelLocalizationKey.localized()
        }
    }
    
    @IBInspectable var secondaryLabelLocalizationKey: String = "" {
        didSet {
            guard secondaryLabelLocalizationKey != "" else {
                secondaryLabelLeadingConstraint.constant = 0
                return
            }
            secondaryLabelLeadingConstraint.constant = 4
            self.secondaryLabel.text = secondaryLabelLocalizationKey.localized()
        }
    }
    
    @IBInspectable var placeholderLocalizationKey: String? = nil {
        didSet {
            if let key = placeholderLocalizationKey {
                self.textField.placeholder = key.localized()
            } else {
                self.textField.placeholder = nil
            }
        }
    }
    
    @IBInspectable var showAsterisk: Bool = false {
        didSet {
            self.asteriskLabel.isHidden = !showAsterisk
        }
    }
    
    // IBInspectable does not support enum; see enum declaration for button types
    @IBInspectable var buttonType: Int = 0 {
        didSet {
            setImage()
            setTextFieldPadding()
        }
    }
    
    @IBInspectable var infoButtonIsHidden: Bool = true {
        didSet {
            infoButton.isHidden = infoButtonIsHidden
        }
    }
    
    @IBInspectable var isNumpad: Bool = false {
        didSet {
            if isNumpad {
                textField.keyboardType = .decimalPad
            }
        }
    }
    
    @IBInspectable var secureTextEntry: Bool = false {
        didSet {
            if secureTextEntry {
                textField.isSecureTextEntry = true
            }
        }
    }
    
    @IBInspectable var setFormatAmount: Bool = false {
        didSet {
            if setFormatAmount {
                textField.addTarget(self, action: #selector(self.editingDidBegin), for: .editingDidBegin)
                textField.addTarget(self, action: #selector(self.editingDidEnd), for: .editingDidEnd)
                textField.addTarget(self, action: #selector(self.editingChanged), for: .editingChanged)
            }
        }
    }
    
    @objc func editingDidBegin(_ textField: UITextField) {
        textField.text = NumberUtil.formatAmount(amountValue)
    }
    
    @objc func editingDidEnd(_ textField: UITextField) {
        textField.text = NumberUtil.formatDecimal(amountValue)
        resetZeroAmount(amountValue)
    }
    
    @objc func editingChanged(_ textField: UITextField) {
        if NumberUtil.validateAmountValue(amountValue: textField.text ?? "", maxValue: maxAmount) {
            amountValue = (textField.text ?? "").filter("0123456789.".contains)
        }
        textField.text = NumberUtil.formatAmount(amountValue)
        DispatchQueue.main.async {
            let newPosition = self.textField.endOfDocument
            self.textField.selectedTextRange = self.textField.textRange(from: newPosition, to: newPosition)
        }
    }
    
    var delegate: UITextFieldDelegate? {
        didSet {
            textField.delegate = delegate
            textField.textRect = infoButton.frame.size.width*2
        }
    }
    
    var infoButtonTapped: ((UIButton) -> Void)?
    var buttonTapped: (() -> Void)?
    
    private var contentView: UIView?
    static let nibName = "TextFieldCard"
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
        setupLocalizationKey()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
        setupLocalizationKey()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        self.backgroundColor = .clear
        setupLocalizationKey()
    }
    
    private func xibSetup() {
        guard let view = loadViewFromNib() else { return }
        view.frame = bounds
        self.addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        view.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        self.backgroundColor = .clear
        view.clipsToBounds = false
        contentView = view
    }

    private func loadViewFromNib() -> UIView? {
        let nibName = TextFieldCard.nibName
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(
                    withOwner: self,
                    options: nil).first as? UIView
    }
    
    private func setupLocalizationKey() {
        self.mainLabel.text = labelLocalizationKey.localized()
        self.secondaryLabel.text = secondaryLabelLocalizationKey.localized()
        if let key = placeholderLocalizationKey {
            self.textField.placeholder = key.localized()
        } else {
            self.textField.placeholder = nil
        }
    }
    
    private func setImage() {
        let type = TextFieldCardButtonType.init(rawValue: buttonType)
        if type == .hidden {
            self.button.isHidden = true
            return
        }
        self.button.isHidden = false
        var image: UIImage?
        switch type {
        case .dropdown:
            image = UIImage(named: "form_down", in: Bundle(for: TextFieldCard.self), compatibleWith: nil)
        case .date:
            image = UIImage(named: "form_schedule", in: Bundle(for: TextFieldCard.self), compatibleWith: nil)
        case .hidden, .none:
            image = nil
        }
        self.button.setImage(image: image)
    }
    
    private func setTextFieldPadding() {
        let type = TextFieldCardButtonType.init(rawValue: buttonType)
        switch type {
        case .dropdown, .date:
            self.textField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: self.button.frame.width, height: 1))
            self.textField.rightViewMode = .always
        case .hidden, .none:
            self.textField.rightViewMode = .never
        }
    }
    
    private func resetZeroAmount(_ str: String) {
        let amount = NumberUtil.removeDoubleDot(str)
        let doubleAmount = Double(amount.replacingOccurrences(of: ",", with: "")) ?? 0.00

        if doubleAmount == 0.00 {
            amountValue = ""
        }
    }
    
    override func becomeFirstResponder() -> Bool {
        return textField.becomeFirstResponder()
    }
    
    override func resignFirstResponder() -> Bool {
        return textField.resignFirstResponder()
    }
    
    @IBAction func infoButtonTouchUpInside(_ sender: Any) {
        self.infoButtonTapped?(self.infoButton)
    }
    
    @IBAction func buttonTouchUpInside(_ sender: Any) {
        self.buttonTapped?()
    }
}
